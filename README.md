# PwaClicker (pwa-clicker)

App mobile progresiva (Clicker)

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```
### For more information: Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Build the app for production
```bash
quasar build -m pwa
```

### Launch tests
```bash
npm run test:unit
```
### Instructions to run the application locally
```bash
Añadir extensión para crear una PWA:

    quasar mode add pwa

Para desplegar:

    quasar build -m pwa 

Como las PWA solo funcionan en "https" se debe instalar httpserver y ngrock para simularlo

    npm install --global http-server

    npm install ngrok

Más información en: https://www.npmjs.com/package/http-server y https://www.npmjs.com/package/ngrok

Para poder utilizarlos, se debe abrir 2 terminales diferentes (ambas apuntando a la raíz del proyecto)

En la primera para lanzar el servidor:

    http-server ./dist

En la otra:

    ngrok http 8080

    Abrirá una consola nueva donde indica entre otros datos la url que necesitamos abrir desde el navegador
    (algo así como "https://4e3b-83-58-10-212.ngrok.io")

    Abrir una ventana del navegador (preferentemente Chrome) añadiendo "/pwa/" a la url indicada.

    Por ejemplo:

        "https://4e3b-83-58-10-212.ngrok.io/pwa/"

```



