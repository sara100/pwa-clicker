import { describe, expect, it } from '@jest/globals';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-jest';
import { mount, shallowMount } from '@vue/test-utils';
import { QBtn } from 'quasar';

import TextLineCustom from '../../../../src/components/TextLineCustom';

// Specify here Quasar config you'll need to test your component
installQuasarPlugin();

describe('Tests de componente TextLineCustom', () => {

    let wrapper

    beforeEach(() => {
        wrapper = shallowMount( TextLineCustom )
    })

    test('Debe renderizar el componente correctamente', () => {
        const { vm } = wrapper;
        expect( wrapper.html() ).toMatchSnapshot()
    })

    it('Propiedad cls por defecto correcta', () => {
        const wrapper = mount(TextLineCustom);
        const { vm } = wrapper;

        expect(vm.cls).toBe('');
        expect(vm.cls).toBe('');
    });
  
    it('Propiedad text por defecto correcta', () => {
        const wrapper = mount(TextLineCustom);
        const { vm } = wrapper;

        expect(typeof vm.text).toBe('string');
        expect(vm.text).toBe('');
    });
});
