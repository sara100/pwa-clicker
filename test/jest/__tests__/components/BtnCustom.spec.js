import { describe, expect, it } from '@jest/globals';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-jest';
import { mount, shallowMount } from '@vue/test-utils';
import { QBtn } from 'quasar';

import BtnCustom from '../../../../src/components/BtnCustom';

// Specify here Quasar config you'll need to test your component
installQuasarPlugin();

describe('Tests de componente BtnCustom', () => {

    let wrapper

    beforeEach(() => {
        wrapper = shallowMount( BtnCustom )
    })

    test('Debe renderizar el componente correctamente', () => {
        const { vm } = wrapper;
        expect( wrapper.html() ).toMatchSnapshot()
    })

    it('Propiedad icon por defecto correcta', () => {
        const wrapper = mount(BtnCustom);
        const { vm } = wrapper;

        expect(vm.icon).toBe(undefined);
    });
  
    it('Propiedad Label por defecto correcta', () => {
        const wrapper = mount(BtnCustom);
        const { vm } = wrapper;

        expect(typeof vm.label).toBe('string');
        expect(vm.label).toBe('Join');
    });

    it('Propiedad type por defecto correcta', () => {
        const wrapper = mount(BtnCustom);
        const { vm } = wrapper;

        expect(typeof vm.type).toBe('string');
        expect(vm.type).toBe('submit');
    });

});
