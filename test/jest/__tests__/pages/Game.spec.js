import { describe, expect, it } from '@jest/globals';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-jest';
import { mount, shallowMount } from '@vue/test-utils';
import { QBtn } from 'quasar';
import Game from '../../../../src/pages/Game';

// Specify here Quasar config you'll need to test your component
installQuasarPlugin();

describe('Pruebas en Game Page', () => {

    let wrapper

    beforeEach(() => {
        wrapper = shallowMount( Game )
    })

    test('debe renderizar el componente Game correctamente', () => {  
        expect( wrapper.html() ).toMatchSnapshot()
    })

    it('Contiene el método addPoint', () => {
        const { vm } = wrapper;    
        expect(typeof vm.addPoint).toBe('function');
    });

    it('Contiene el método buyPoints', () => {
        const { vm } = wrapper;    
        expect(typeof vm.buyPoints).toBe('function');
    });

    it('Contiene el método saveUserData', () => {
        const { vm } = wrapper;    
        expect(typeof vm.saveUserData).toBe('function');
    });
    
});
