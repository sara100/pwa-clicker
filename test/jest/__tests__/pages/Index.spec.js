import { describe, expect, it } from '@jest/globals';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-jest';
import { mount, shallowMount } from '@vue/test-utils';
import { QBtn } from 'quasar';
import PageIndex from '../../../../src/pages/Index';

// Specify here Quasar config you'll need to test your component
installQuasarPlugin();

describe('Pruebas en el Index Page', () => {

    let wrapper

    beforeEach(() => {
        wrapper = shallowMount( PageIndex )
    })

    test('Debe renderizar el componente correctamente', () => {
      
        const { vm } = wrapper;
        expect( wrapper.html() ).toMatchSnapshot()
    })

    it('Contiene el método saveEntry', () => {
        const { vm } = wrapper;
    
        expect(typeof vm.saveEntry).toBe('function');
    });

    it('Contiene el método onReset', () => {
        const { vm } = wrapper;
    
        expect(typeof vm.onReset).toBe('function');
    });
});
