const aux = (s) => {
    s.count ++
    s.showAutoMergers = ( s.count >= s.autoMergerPrice || s.numAutoClickers > 0 ) ? true : false
    s.autoClickerCost = s.autoMergerPrice + s.autoMergerPrice * s.numAutoClickers
    s.disableBtn = ( s.count < s.autoClickerCost ) ? true : false
}

let interv

export const increment = ( state ) => {
    const s = state    
    aux(s)
}

export const buyPoints = ( state ) => {
    const s = state
    s.numAutoClickers ++
    s.count = s.count - s.autoClickerCost
    s.disableBtn = true
    const interval = 100

    interv = setInterval( () => {
        aux(s)      
    }, interval)
}
export const clearAutoPoints = ( state ) => {
    clearInterval(interv);
}

export const updateUser = ( state, 
        { autoClickerCost, autoMergerPrice, count, disableBtn, numAutoClickers, showAutoMergers } ) => {
    const s = state
    s.autoClickerCost = autoClickerCost
    s.autoMergerPrice = autoMergerPrice
    s.count = count
    s.disableBtn = disableBtn
    s.disableBtn = disableBtn
    s.numAutoClickers = numAutoClickers
    s.showAutoMergers = showAutoMergers
}

export const updateName = ( state, name ) => {
    const s = state
    s.name = name
}
