

export default () => ({
  autoClickerCost: 50,
  autoMergerPrice: 50,
  count: 1,
  disableBtn: false,
  name: '',
  numAutoClickers: 0,
  showAutoMergers: false,
})
